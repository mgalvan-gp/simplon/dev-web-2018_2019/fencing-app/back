<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowTeams()
    {
        $crawler = $this->client->request('GET', 'api-fencing/teams');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testShowOneTeam()
    {
        $crawler = $this->client->request('GET', 'api-fencing/teams/1');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
}
