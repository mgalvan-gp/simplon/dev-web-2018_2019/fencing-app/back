<?php

namespace App\Repository;

use App\Entity\Fencer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Fencer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fencer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fencer[]    findAll()
 * @method Fencer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FencerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Fencer::class);
    }

    // /**
    //  * @return Fencer[] Returns an array of Fencer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fencer
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
