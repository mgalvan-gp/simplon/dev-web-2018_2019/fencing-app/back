<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\EventRepository;

class EventControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowEvents()
    {
        $crawler = $this->client->request('GET', 'api-fencing/events');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testShowOneEvent()
    {
        $crawler = $this->client->request('GET', 'api-fencing/events/1');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
}
