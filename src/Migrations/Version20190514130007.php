<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190514130007 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, location VARCHAR(255) DEFAULT NULL, start DATE DEFAULT NULL, end DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_fencer (event_id INT NOT NULL, fencer_id INT NOT NULL, INDEX IDX_3BCDB83071F7E88B (event_id), INDEX IDX_3BCDB830D76841B (fencer_id), PRIMARY KEY(event_id, fencer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_season (event_id INT NOT NULL, season_id INT NOT NULL, INDEX IDX_C27D2CFB71F7E88B (event_id), INDEX IDX_C27D2CFB4EC001D1 (season_id), PRIMARY KEY(event_id, season_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_weapon_type (event_id INT NOT NULL, weapon_type_id INT NOT NULL, INDEX IDX_9F486C5671F7E88B (event_id), INDEX IDX_9F486C56607BCCD7 (weapon_type_id), PRIMARY KEY(event_id, weapon_type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_team (event_id INT NOT NULL, team_id INT NOT NULL, INDEX IDX_DB2BEAB471F7E88B (event_id), INDEX IDX_DB2BEAB4296CD8AE (team_id), PRIMARY KEY(event_id, team_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event_fencer ADD CONSTRAINT FK_3BCDB83071F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_fencer ADD CONSTRAINT FK_3BCDB830D76841B FOREIGN KEY (fencer_id) REFERENCES fencer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_season ADD CONSTRAINT FK_C27D2CFB71F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_season ADD CONSTRAINT FK_C27D2CFB4EC001D1 FOREIGN KEY (season_id) REFERENCES season (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_weapon_type ADD CONSTRAINT FK_9F486C5671F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_weapon_type ADD CONSTRAINT FK_9F486C56607BCCD7 FOREIGN KEY (weapon_type_id) REFERENCES weapon_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_team ADD CONSTRAINT FK_DB2BEAB471F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_team ADD CONSTRAINT FK_DB2BEAB4296CD8AE FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_fencer DROP FOREIGN KEY FK_3BCDB83071F7E88B');
        $this->addSql('ALTER TABLE event_season DROP FOREIGN KEY FK_C27D2CFB71F7E88B');
        $this->addSql('ALTER TABLE event_weapon_type DROP FOREIGN KEY FK_9F486C5671F7E88B');
        $this->addSql('ALTER TABLE event_team DROP FOREIGN KEY FK_DB2BEAB471F7E88B');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE event_fencer');
        $this->addSql('DROP TABLE event_season');
        $this->addSql('DROP TABLE event_weapon_type');
        $this->addSql('DROP TABLE event_team');
    }
}
